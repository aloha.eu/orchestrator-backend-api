from werkzeug.security import check_password_hash, generate_password_hash
from flask_mongoengine import MongoEngine
import datetime
from mongoengine.fields import *

db = MongoEngine()


class User(db.Document):
    email = db.StringField(max_length=120)
    # _password = db.StringField(max_length=256)
    project = db.ListField(db.StringField(max_length=64))
    creation_date = db.DateTimeField()
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    def __init__(self, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        # self.login = db.StringField(max_length=80, unique=True)
        # self.email = db.StringField(max_length=120)
        self._password = db.StringField(max_length=256)
        # self.project = db.ListField(db.StringField(max_length=64))
        # self.creation_date = db.DateTimeField()
        # self.modified_date = db.DateTimeField(default=datetime.datetime.now)

    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(id)

    @property
    def password(self):
        print("getter of x password called")
        return self._password

    @password.setter
    def password(self, value):
        self._password = generate_password_hash(value)

    @staticmethod
    def validate_login(password_hash, password):
        return check_password_hash(password_hash, password)


class Card(db.Document):
    title = StringField(max_length=32)
    label = StringField(max_length=32)
    description = StringField(max_length=500)
    laneId = ObjectIdField(required=False)
    projectId = ObjectIdField(required=False)

    def get_id(self):
        return str(self.id)


class Lane(db.Document):
    position = IntField()
    title = StringField(max_length=32)
    cards = ListField(ReferenceField(Card, required=False))

    def get_id(self):
        return str(self.id)


class Project(db.Document):

    #creator = db.StringField()
    dataset = db.StringField()
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    def get_id(self):
        return str(self.id)


# Contrains use case
class ProjectContrains(db.Document):
    #name = db.StringField(max_length=64) #Restricted set of values. Power, Security_level, Execution time, Accuracy
    power_value = db.IntField()
    power_priority = db.IntField()

    security_value = db.IntField()
    security_priority = db.IntField()

    execution_time_value = db.IntField()
    execution_time_priority = db.IntField()

    accuracy_value = db.IntField()
    accuracy_priority = db.IntField()


    project = db.StringField()
    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


#Conf file
class ConfigurationFile(db.Document):
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    project = db.StringField()
    mode = db.StringField()
    data_set_name = db.StringField()
    data_dir = db.StringField()
    lr = db.FloatField()
    lr_decay = db.FloatField()
    ref_steps = db.IntField()
    ref_patience = db.IntField()
    batch_size = db.IntField()
    num_epochs = db.IntField()
    loss =  db.StringField()
    optimizer =  db.StringField()
    image_dimensions = db.ListField()
    num_classes = db.IntField()
    num_filters = db.IntField()
    upconv =  db.StringField()
    nonlin =  db.StringField()
    task_type =  db.StringField()
    accuracy =  db.StringField()
    augmentation = db.DictField()
    data_split = db.FloatField()
    normalize = db.BooleanField()
    zero_center = db.BooleanField()

    # path = db.StringField()


    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


class AlgorithmConfiguration(db.Document):
    creation_date   = db.DateTimeField(default=datetime.datetime.now)
    modified_date   = db.DateTimeField(default=datetime.datetime.now)

    project         = db.StringField()
    onnx            = db.StringField() # the path of the onnx model before the training


    # training
    onnx_trained        = db.StringField() # the path of the trained onnx model
    pytorch_trained     = db.StringField() # the path of the trained pytotch model

    training_accuracy   = db.FloatField()
    training_loss       = db.FloatField()
    validation_accuracy = db.FloatField()
    validation_loss     = db.FloatField()


    # refinemente parsimonious inference
    rpi_onnx_path           = db.StringField() # the path for the onnx model after the rpi
    rpi_training_accuracy   = db.FloatField()
    rpi_training_loss       = db.FloatField()
    rpi_onnx_img_path       = db.StringField()


    # power performance evaluation
    performance    = db.FloatField()
    energy         = db.FloatField()
    processors     = db.FloatField()
    memory         = db.FloatField()


    # security
    sec_level      = db.StringField() #low, medium, high
    sec_value      = db.FloatField()
    sec_curve      = db.DictField()


    # images from tools
    training_img_path        = db.StringField()
    power_perf_eval_img_path = db.StringField()
    rpi_img_path             = db.StringField()
    security_img_path        = db.StringField()

    def clean(self):
        self.modified_date = datetime.datetime.now()

    def get_id(self):
        return str(self.id)


class Architecture(db.Document): #File
    project = db.StringField()
    creation_date = db.DateTimeField()
    path = db.StringField()


    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)

#Dataset
class Dataset(db.Document):
    name = db.StringField()
    project = db.StringField()
    path = db.StringField()
    data_resolution = db.ListField()
    number_labels = db.IntField()
    training_flag = db.ListField()
    labels = db.ListField()
    features = db.ListField()

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


#Conf file
class ConfigurationFile(db.Document):
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    project = db.StringField()
    mode = db.StringField()
    data_set_name = db.StringField()
    data_dir = db.StringField()
    lr = db.FloatField()
    lr_decay = db.FloatField()
    ref_steps = db.IntField()
    ref_patience = db.IntField()
    batch_size = db.IntField()
    num_epochs = db.IntField()
    loss =  db.StringField()
    optimizer =  db.StringField()
    image_dimensions = db.ListField()
    num_classes = db.IntField()
    num_filters = db.IntField()
    upconv =  db.StringField()
    nonlin =  db.StringField()
    task_type =  db.StringField()
    accuracy =  db.StringField()
    augmentation = db.DictField()
    data_split = db.FloatField()
    normalize = db.BooleanField()
    zero_center = db.BooleanField()

    # path = db.StringField()


    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


class Architecture(db.Document): #File
    project = db.StringField()
    creation_date = db.DateTimeField()
    path = db.StringField()


    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)
