# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g
from flask import current_app as app


from models.models import Project, Card, Lane

from . import Resource
from .. import schemas
import requests
from flask_socketio import emit

from bson import ObjectId



class ProjectsExecutionStart(Resource):

    def post(self):


        """
        STEP 1: RETRIEVE PROJECT AND CARD FROM DB
        """
        print(request.get_json())

        try:
            projectId = ObjectId(request.get_json()['projectId'])
        except Exception as ex: # check the id, it must be a 24digit hex number
            print ("ERROR! Wrong id format")
            print(str(ex))
            return {"message": "Wrong projectId format. It must be a 24digit hex number", "code": 400}, 400, {}

        proj = Project.objects.get(id=projectId)
        card = Card.objects.get(projectId=projectId)
        first_lane = Lane.objects.get(position=0)


        print(app.config['DSE_URL'])
        dse_url = app.config['DSE_URL']

        print("CARD LANE ID: ", card.laneId)
        print("LANE ID: ", first_lane.get_id())
        print(str(card.laneId) == str(first_lane.get_id()))


        if str(card.laneId) == str(first_lane.get_id()):

            print("Card is in first lane")


            """
            STEP 2: CALL DSE TO START JOB
            """

            dse_start_process_url = "%s/api/start_process?project_id=%s" % (dse_url, proj.get_id())

            try:

                print("Requests to: ", dse_start_process_url)
                r = requests.post(dse_start_process_url)
                print(r.status_code, r.reason)

                start_process_response_json = r.json()


                """
                STEP 3: GET STATUS OF OBTAINED JOB
                """

                dse_status_url = "%s/api/status?job_id=%s" % (dse_url,start_process_response_json['job_id'])

                r2 = requests.get(dse_status_url)

                print("R2: ", r2.json())




                """
                STEP 4: CREATE RESPONSE TO SEND AN EVENT ON THE SOCKET
                """

                response = {
                    'jobId': str(request.get_json()['projectId']),
                    'status': r2.json()['status'],
                    'cardId' : str(card.get_id()),
                    'laneId' :  str(card.laneId)
                }

                print("Response: ")
                print(response)

                emit('status', response, namespace='/', broadcast=True)

                return response, 200, None
            except requests.exceptions.ConnectionError as ex:
                return {"message":"DSE engine is not reachable. Is it running and available at %s?" % dse_url}, 500, {} #Not implemented

        else:
            return {"message":"No action configured for executing project in this stage. This functionality will be added soon."}, 501, {} #Not implemented