# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from bson import ObjectId
from flask import request
from flask_socketio import emit


from models.models import Card, Lane
from . import Resource


def update_lane_id_refs(card_json, card_mongo):
    """
    Updates the cards list in the lanes to which and from which this Card was moved
    :param card_json: the updated Card, as given by the request
    :param card_mongo: the previously stored version of the Card stored in mongo
    """
    if 'laneId' in card_json:
        new_lane_id = ObjectId(card_json['laneId'])
        old_lane_id = card_mongo.laneId
        if old_lane_id == new_lane_id:
            return
        added = Lane.objects(id=new_lane_id).update(add_to_set__cards=card_mongo)
        if added and old_lane_id:
            Lane.objects(id=old_lane_id).update(pull__cards=card_mongo)


class CardsCardid(Resource):

    def get(self, cardId):
        """
        Get a specific Card
        :param cardId: the ID of the particular Card
        :return: the Card identified by cardId
        """
        cards = Card.objects.get(id=cardId)
        return cards, 200, {}

    def post(self, cardId):
        """
        Update a specific Card
        :param cardId:  the ID of the particular Card
        :return:  the Card identified by cardId after the update was performed
        """
        card = request.get_json()
        print(card)
        card_id = ObjectId(cardId)
        card_to_save = Card.objects(id=card_id)[0]
        update_lane_id_refs(card, card_to_save)
        card_update_params = {'set__' + key: value for key, value in card.items()}
        card_to_save.update(**card_update_params)
        print(card_to_save)
        card_to_save.reload()

        data_to_send = card
        emit('card_status', data_to_send, namespace='/', broadcast=True)

        return card_to_save, 200, None
