# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from bson import ObjectId
from flask import request, g

from models.models import Project, Project, Lane
from . import Resource
from .. import schemas


class ProjectsProjectid(Resource):

    def get(self, projectId):
        """
        Get a specific Project
        :param projectId: the ID of the particular Project
        :return: the Project identified by projectId
        """
        project = Project.objects.get(id=projectId)
        print(project.to_json())
        return project, 200, {}

    def post(self, projectId):
        """
        Update a specific Project
        :param projectId:  the ID of the particular Project
        :return:  the Project identified by projectId after the update was performed
        """
        project = request.get_json()
        project_id = ObjectId(projectId)
        print(project)
        project_update_params = {'set__' + key: value for key, value in project.items()}
        project_to_save = Project.objects(id=project_id).update(**project_update_params)
        print(project_to_save)
        return None, 201, None
