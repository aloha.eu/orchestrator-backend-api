# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g

from . import Resource
from .. import schemas
from models.models import AlgorithmConfiguration

class ProjectsExecutionResultProjectid(Resource):

    def get(self, projectId):

        print("PROJECTID: ", projectId)

        try:
            algConf = AlgorithmConfiguration.objects(project=projectId)
            return algConf, 200, None


        except Exception as ex:
            return{"message" : "Error creating ConfigurationFile for given project information:  %s" % str(ex)}, 400, {}