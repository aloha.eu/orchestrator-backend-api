# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request

from models.models import Lane
from . import Resource


class Lanes(Resource):

    def get(self):
        """
       Get all cards
       :return: a list of Cards
       """
        lanes = Lane.objects
        print(lanes.to_json())
        return lanes, 200, {}

    def post(self):
        """
         Create a card
         :return: a Card representation equal to the one stored in Mongo, including its id
         """
        lane = request.get_json()
        print(lane)
        lane_ret = Lane(**lane).save()
        print(lane_ret)
        return lane_ret, 201, None
