# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request

from models.models import Card, Lane
from . import Resource


class Cards(Resource):
    
    def get(self):
        """
        Get all cards
        :return: a list of Cards
        """
        cards = Card.objects
        return cards, 200, {}

    def post(self):
        """
        Create a card
        :return: a Card representation equal to the one stored in Mongo, including its id
        """
        card = request.get_json()
        print(card)
        card_ret = Card(**card).save()
        #Add the card to a Lane if given a laneId
        if 'laneId' in card:
            Lane.objects(id=card['laneId']).update(add_to_set__cards=card_ret)
        print(card_ret)
        return card_ret, 200, None