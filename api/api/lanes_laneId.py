# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from bson import ObjectId
from flask import request

from models.models import Lane
from . import Resource


class LanesLaneid(Resource):

    def get(self, laneId):
        """
        Get a specific Lane
        :param laneId: the ID of the particular Lane
        :return: the Lane identified by laneId
        """
        lane = Lane.objects.get(id=laneId)
        print(lane.to_json())
        return lane, 200, {}

    def post(self, laneId):
        """
        Update a specific Lane
        :param laneId:  the ID of the particular Lane
        :return:  the Lane identified by laneId after the update was performed
        """
        lane = request.get_json()
        lane_id = ObjectId(laneId)
        print(lane)
        lane_update_params = {'set__' + key: value for key, value in lane.items()}
        lane_to_save = Lane.objects(id=lane_id).update(**lane_update_params)
        print(lane_to_save)
        return None, 201, None
