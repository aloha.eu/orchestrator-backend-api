# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g, json

from flask_socketio import emit

from models.models import Project, Card, Lane, ConfigurationFile, ProjectContrains
from . import Resource
from .. import schemas


class Projects(Resource):

    def get(self):
        """
        Get all projects
        :return: a list of projects
        """
        projects = Project.objects
        return projects, 200, {}

    def post(self):
        """
        Create a project
        :return: a Project representation equal to the one stored in Mongo, including its id
        """

        attrs = request.form.to_dict()
        print(attrs)


        CARD_DEFAULT_POSITION = 0

        try:
            proj = Project()
            proj.dataset = attrs['data_set_name']
            proj.save()

            try:

                first_lane = Lane.objects.get(position=CARD_DEFAULT_POSITION)

                card = Card()
                card.laneId = first_lane.get_id()
                card.description = attrs['description']
                card.title = attrs['title']
                card.label = "Ready" ## Change this
                card.projectId = proj.get_id()

                card.save()

                first_lane.cards.append(card)

                first_lane.save()

                for c in first_lane.cards:
                    print(c)



                emit('new_project', json.dumps(card), namespace='/', broadcast=True)



                try:

                    config = ConfigurationFile()

                    config.project = proj.get_id()
                    config.mode = attrs['mode']
                    config.data_set_name = attrs['data_set_name']
                    config.data_dir = attrs['dataset_dir']
                    config.lr = float(attrs['lr'])
                    config.lr_decay = float(attrs['lr_decay'])
                    config.ref_steps = attrs['ref_steps']
                    config.ref_patience = attrs['ref_patience']
                    config.batch_size = attrs['batch_size']
                    config.num_epochs = attrs['num_epochs']
                    config.loss = attrs['loss']
                    config.optimizer = attrs['optimizer']
                    config.image_dimensions = [attrs['image_width'], attrs['image_height'], attrs['image_number_of_channels']]
                    config.num_classes = int(attrs['num_classes'])
                    config.num_filters = attrs['num_filters']
                    config.upconv = attrs['upconv']
                    config.nonlin = attrs['nonlin']
                    config.task_type = attrs['task_type']
                    config.accuracy = attrs['accuracy']
                    config.augmentation = {"augmentation_horizontal" : attrs['augmentation_horizontal'], "augmentation_vertical" : attrs['augmentation_vertical']}
                    config.data_split = float()
                    config.normalize = bool(attrs['normalize'])
                    config.zero_center = bool(attrs['zero_center'])

                    config.save()

                    try:

                        constrains = ProjectContrains()
                        constrains.power_value = int(attrs['power_value'])
                        constrains.power_priority = int(attrs['power_priority'])

                        constrains.security_value = int(attrs['security_value'])
                        constrains.security_priority = int(attrs['security_priority'])

                        constrains.execution_time_value = int(attrs['execution_time_value'])
                        constrains.execution_time_priority = int(attrs['execution_time_priority'])

                        constrains.accuracy_value = int(attrs['accuracy_value'])
                        constrains.accuracy_priority = int(attrs['accuracy_priority'])

                        constrains.save()

                        return proj, 201, None
                    except Exception as ex4:

                        print("Error creating constrains with given information: %s" % str(ex4))

                        return{"message" : "Error creating constrains for given project information:  %s" % str(ex4)}, 400, {}
                except Exception as ex3:
                    print("Error creating config with given information: %s" % str(ex3))

                    return{"message" : "Error creating ConfigurationFile for given project information:  %s" % str(ex3)}, 400, {}


            except Exception as ex2:
                print("Error creating card with given information: %s" % str(ex2))

                return{"message" : "Error creating card for given project information: %s" % str(ex2)}, 400, {}
        except Exception as ex:

            print("Error creating project with given information: %s" % str(ex))

            return{"message" : "Error creating project with given information: %s" % str(ex)}, 400, {}

