# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function
from flask import current_app as app
from flask import send_from_directory, send_file
import os

from flask import request


from . import Resource


class ProjectsFile(Resource):
    
    def get(self):
        """
        Get a file from given path
        :return: a file
        """

        try:

            shared_folder_path = app.config['SHARED_FOLDER_PATH']

            if(shared_folder_path != ""):
                filePath = os.path.join(shared_folder_path, request.args.get('filePath'))
            else:
                filePath = request.args.get('filePath')


            if not os.path.isfile(filePath):
                return{"message" : "No file found in given path:  %s" % str(filePath)}, 400, {}


            else:
                #return send_from_directory(filePath)

                return send_file(filePath)
                #return app.send_static_file(filePath)


        except Exception as ex:
            return{"message" : "Error reading file from given path:  %s" % str(ex)}, 400, {}

