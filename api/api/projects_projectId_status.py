# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

import flask
import socketio
from bson import ObjectId
from flask import request, g, current_app, json
from flask_socketio import emit

from models.models import Project, Card, Lane

from . import Resource
from .. import schemas


class ProjectsProjectidStatus(Resource):

    def post(self, projectId):
        """
        Update the status of a project
        Note the status is stored as card.label, but the operation is implemented to give modules the ability to
            use projectId as lookup key
        :param projectId: the id of the particular project
        :return: the full Card that holds the display info for this Project, after the update has been performed 
        """

        project_id = ObjectId(projectId)
        card_to_update = Card.objects(projectId=project_id)[0]
        status = request.get_json()
        card_to_update.update(set__label=status['status'])
        data_to_send = json.loads(card_to_update.to_json())
        print(data_to_send)
        data_to_send['laneId'] = data_to_send['laneId']['$oid']
        data_to_send['projectId'] = data_to_send['projectId']['$oid']
        data_to_send['id'] = data_to_send['_id']['$oid']
        data_to_send['label'] = status['status']
        del data_to_send['_id']
        print(data_to_send)
        emit('status', data_to_send, namespace='/', broadcast=True)
        return data_to_send, 200, None
