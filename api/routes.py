# -*- coding: utf-8 -*-

###
### DO NOT CHANGE THIS FILE
###
### The code is auto generated, your change will be overwritten by
### code generating.
###
from __future__ import absolute_import

from .api.cards import Cards
from .api.cards_cardId import CardsCardid
from .api.lanes import Lanes
from .api.lanes_laneId import LanesLaneid
from .api.projects import Projects
from .api.projects_projectId import ProjectsProjectid
from .api.projects_projectId_status import ProjectsProjectidStatus
from .api.projects_execution_result_projectId import ProjectsExecutionResultProjectid
from .api.projects_execution_start import ProjectsExecutionStart
from .api.project_files import ProjectsFile


routes = [
    dict(resource=Cards, urls=['/cards'], endpoint='cards'),
    dict(resource=CardsCardid, urls=['/cards/<cardId>'], endpoint='cards_cardId'),
    dict(resource=Lanes, urls=['/lanes'], endpoint='lanes'),
    dict(resource=LanesLaneid, urls=['/lanes/<laneId>'], endpoint='lanes_laneId'),
    dict(resource=Projects, urls=['/projects'], endpoint='projects'),
    dict(resource=ProjectsProjectid, urls=['/projects/<projectId>'], endpoint='projects_projectId'),
    dict(resource=ProjectsProjectidStatus, urls=['/projects/<projectId>/status'], endpoint='projects_projectId_status'),
    dict(resource=ProjectsExecutionResultProjectid, urls=['/projects/execution/result/<projectId>'], endpoint='projects_execution_result_projectId'),
    dict(resource=ProjectsExecutionStart, urls=['/projects/execution/start'], endpoint='projects_execution_start'),
    dict(resource=ProjectsFile, urls=['/projects/file'], endpoint='projects_file'),
]