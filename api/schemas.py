# -*- coding: utf-8 -*-

import six

# TODO: datetime support

###
### DO NOT CHANGE THIS FILE
### 
### The code is auto generated, your change will be overwritten by 
### code generating.
###

base_path = '/api'



DefinitionsCard = {'required': ['title', 'label'], 'properties': {'id': {'type': 'string'}, 'title': {'type': 'string'}, 'label': {'type': 'string'}, 'description': {'type': 'string'}, 'laneId': {'type': 'string'}, 'projectId': {'type': 'string'}}}
DefinitionsError = {'required': ['code', 'message'], 'properties': {'code': {'type': 'integer', 'format': 'int32'}, 'message': {'type': 'string'}}}
DefinitionsLanewithcardids = {'properties': {'id': {'type': 'string'}, 'position': {'type': 'integer'}, 'title': {'type': 'string'}, 'label': {'type': 'string'}, 'cards': {'type': 'array', 'items': {'type': 'string'}}}}
DefinitionsResult = {'required': ['id', 'values'], 'properties': {'id': {'type': 'string'}, 'values': {'type': 'array', 'items': {'type': 'integer', 'format': 'int32'}}}}
DefinitionsStatus = {'required': ['jobId', 'status'], 'properties': {'projectId': {'type': 'string'}, 'status': {'type': 'string'}}}
DefinitionsStartedexecution = {'required': ['jobId'], 'properties': {'jobId': {'type': 'string'}}}
DefinitionsProject = {'required': ['title'], 'properties': {'dataset': {'type': 'string'}}}
DefinitionsLane = {'required': ['title', 'position'], 'properties': {'id': {'type': 'string'}, 'position': {'type': 'integer'}, 'title': {'type': 'string'}}}
DefinitionsLanes = {'type': 'array', 'items': {'required': ['title', 'position'], 'properties': {'id': {'type': 'string'}, 'position': {'type': 'integer'}, 'title': {'type': 'string'}}}}
DefinitionsResults = {'type': 'array', 'items': {'required': ['id', 'values'], 'properties': {'id': {'type': 'string'}, 'values': {'type': 'array', 'items': {'type': 'integer', 'format': 'int32'}}}}}
DefinitionsProjects = {'type': 'array', 'items': {'required': ['title'], 'properties': {'dataset': {'type': 'string'}}}}
DefinitionsCards = {'type': 'array', 'items': {'required': ['title', 'label'], 'properties': {'id': {'type': 'string'}, 'title': {'type': 'string'}, 'label': {'type': 'string'}, 'description': {'type': 'string'}, 'laneId': {'type': 'string'}, 'projectId': {'type': 'string'}}}}
DefinitionsLanewithcards = {'properties': {'id': {'type': 'string'}, 'position': {'type': 'integer'}, 'title': {'type': 'string'}, 'label': {'type': 'string'}, 'cards': {'type': 'array', 'items': {'required': ['title', 'label'], 'properties': {'id': {'type': 'string'}, 'title': {'type': 'string'}, 'label': {'type': 'string'}, 'description': {'type': 'string'}, 'laneId': {'type': 'string'}, 'projectId': {'type': 'string'}}}}}}


validators = {
    ('cards', 'POST'): {'json': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}},
    ('cards_cardId', 'POST'): {'json': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}},
    ('projects_projectId', 'POST'): {'json': {'required': ['title'], 'properties': {'description': {'type': 'string'}, 'title': {'type': 'string'}, 'dataset': {'type': 'string'}, 'architectureSpec': {'type': 'string'}, 'algorithmConfig': {'type': 'string'}, 'config': {'type': 'string'}, 'id': {'type': 'string'}, 'constraints': {'type': 'string'}}}},
    ('projects_projectId_status', 'POST'): {'json': {'properties': {'status': {'type': 'string'}, 'log': {'type': 'string'}}}},
    ('projects', 'POST'): {'form': {'required': [], 'properties': {'description': {'type': 'string'}, 'title': {'type': 'string'}, 'dataset': {'type': 'string'}, 'architectureSpec': {'type': 'file', 'description': 'The architecture spec file to upload.'}, 'algorithmConfig': {'type': 'file', 'description': 'The algorithm config file to upload.'}, 'config': {'type': 'file', 'description': 'The config file to upload.'}}}},
    ('lanes', 'POST'): {'json': {'required': ['title', 'position'], 'properties': {'position': {'type': 'integer'}, 'id': {'type': 'string'}, 'title': {'type': 'string'}}}},
    ('lanes_laneId', 'POST'): {'json': {'properties': {'cards': {'items': {'type': 'string'}, 'type': 'array'}, 'position': {'type': 'integer'}, 'title': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}},
    ('projects_projectId_status', 'POST'): {'json': {'properties': {'status': {'type': 'string'}, 'log': {'type': 'string'}}}},
    ('projects_execution_start', 'POST'): {'json': {'properties': {'projectId': {'type': 'string'}}}},
}

filters = {
    ('cards', 'POST'): {201: {'headers': None, 'schema': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('cards', 'GET'): {200: {'headers': {'x-next': {'type': 'string', 'description': 'A link to the next page of responses'}}, 'schema': {'items': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}, 'type': 'array'}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('cards_cardId', 'POST'): {200: {'headers': None, 'schema': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('cards_cardId', 'GET'): {200: {'headers': None, 'schema': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('projects_projectId', 'POST'): {200: {'headers': None, 'schema': {'required': ['title'], 'properties': {'description': {'type': 'string'}, 'title': {'type': 'string'}, 'dataset': {'type': 'string'}, 'architectureSpec': {'type': 'string'}, 'algorithmConfig': {'type': 'string'}, 'config': {'type': 'string'}, 'id': {'type': 'string'}, 'constraints': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('projects_projectId', 'GET'): {200: {'headers': None, 'schema': {'required': ['title'], 'properties': {'description': {'type': 'string'}, 'title': {'type': 'string'}, 'dataset': {'type': 'string'}, 'architectureSpec': {'type': 'string'}, 'algorithmConfig': {'type': 'string'}, 'config': {'type': 'string'}, 'id': {'type': 'string'}, 'constraints': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('projects_projectId_status', 'POST'): {200: {'headers': None, 'schema': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('projects', 'POST'): {201: {'headers': None, 'schema': {'required': [], 'properties': {'dataset': {'type': 'string'}, 'id': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('projects', 'GET'): {200: {'headers': {'x-next': {'type': 'string', 'description': 'A link to the next page of responses'}}, 'schema': {'items': {'required': ['title'], 'properties': {'description': {'type': 'string'}, 'title': {'type': 'string'}, 'dataset': {'type': 'string'}, 'architectureSpec': {'type': 'string'}, 'algorithmConfig': {'type': 'string'}, 'config': {'type': 'string'}, 'id': {'type': 'string'}, 'constraints': {'type': 'string'}}}, 'type': 'array'}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('lanes', 'POST'): {201: {'headers': None, 'schema': {'required': ['title', 'position'], 'properties': {'position': {'type': 'integer'}, 'id': {'type': 'string'}, 'title': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('lanes', 'GET'): {200: {'headers': {'x-next': {'type': 'string', 'description': 'A link to the next page of responses'}}, 'schema': {'items': {'properties': {'cards': {'items': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}, 'type': 'array'}, 'position': {'type': 'integer'}, 'title': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}, 'type': 'array'}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('lanes_laneId', 'POST'): {200: {'headers': None, 'schema': {'required': ['title', 'position'], 'properties': {'position': {'type': 'integer'}, 'id': {'type': 'string'}, 'title': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('lanes_laneId', 'GET'): {200: {'headers': None, 'schema': {'properties': {'cards': {'items': {'required': ['title', 'label'], 'properties': {'laneId': {'type': 'string'}, 'description': {'type': 'string'}, 'title': {'type': 'string'}, 'projectId': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}, 'type': 'array'}, 'position': {'type': 'integer'}, 'title': {'type': 'string'}, 'id': {'type': 'string'}, 'label': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},


    ('projects_execution_result_projectId', 'GET'): {200: {'headers': None, 'schema': {'type': 'array', 'items': {'required': ['id'], 'properties': {'id': {'type': 'string'}, 'creation_date' : {'type':'string'},'modified_date' : {'type':'string'},'project' : {'type':'string'},'onnx' : {'type':'string'},'onnx_trained' : {'type':'string'},  'pytorch_trained' : {'type':'string'},  'training_accuracy' : {'type':'float'},'training_loss' : {'type':'float'}, 'validation_accuracy' : {'type':'float'}, 'validation_loss' : {'type':'float'},  'rpi_onnx_path' : {'type':'string'},  'rpi_training_accuracy' : {'type':'float'},'rpi_training_loss' : {'type':'float'}, 'rpi_onnx_img_path' : {'type':'string'}, 'performance' : {'type':'float'}, 'energy' : {'type':'float'},'processors' : {'type':'float'},  'memory' : {'type':'float'},'sec_level' : {'type':'string'},'sec_value' : {'type':'float'},'sec_curve' : {'type':'string'},'training_img_path' : {'type':'string'},  'power_perf_eval_img_path' : {'type':'string'}, 'rpi_img_path' : {'type':'string'}, 'security_img_path' : {'type':'string'}}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('projects_execution_start', 'POST'): {200: {'headers': None, 'schema': {'required': [], 'properties': {'jobId': {'type': 'string'}, 'status': {'type': 'string'}, 'cardId': {'type': 'string'}, 'laneId': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}, 400: {'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

    ('projects_file', 'GET'): {200: {'headers': None, 'schema': {'required': [], 'properties': {'jobId': {'type': 'string'}, 'status': {'type': 'string'}, 'cardId': {'type': 'string'}, 'laneId': {'type': 'string'}}}}, 500:{'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}, 400: {'headers': None, 'schema': {'required': [], 'properties': {'message': {'type': 'string'}}}}},

}

scopes = {
}


class Security(object):

    def __init__(self):
        super(Security, self).__init__()
        self._loader = lambda: []

    @property
    def scopes(self):
        return self._loader()

    def scopes_loader(self, func):
        self._loader = func
        return func

security = Security()


def merge_default(schema, value, get_first=True):
    # TODO: more types support
    type_defaults = {
        'integer': 9573,
        'string': 'something',
        'object': {},
        'array': [],
        'boolean': False
    }

    results = normalize(schema, value, type_defaults)
    if get_first:
        return results[0]
    return results


def normalize(schema, data, required_defaults=None):
    if required_defaults is None:
        required_defaults = {}
    errors = []

    class DataWrapper(object):

        def __init__(self, data):
            super(DataWrapper, self).__init__()
            self.data = data

        def get(self, key, default=None):
            if isinstance(self.data, dict):
                return self.data.get(key, default)
            return getattr(self.data, key, default)

        def has(self, key):
            if isinstance(self.data, dict):
                return key in self.data
            return hasattr(self.data, key)

        def keys(self):
            if isinstance(self.data, dict):
                return list(self.data.keys())
            return list(getattr(self.data, '__dict__', {}).keys())

        def get_check(self, key, default=None):
            if isinstance(self.data, dict):
                value = self.data.get(key, default)
                has_key = key in self.data
            else:
                try:
                    value = getattr(self.data, key)
                except AttributeError:
                    value = default
                    has_key = False
                else:
                    has_key = True
            return value, has_key

    def _merge_dict(src, dst):
        for k, v in six.iteritems(dst):
            if isinstance(src, dict):
                if isinstance(v, dict):
                    r = _merge_dict(src.get(k, {}), v)
                    src[k] = r
                else:
                    src[k] = v
            else:
                src = {k: v}
        return src

    def _normalize_dict(schema, data):
        result = {}
        if not isinstance(data, DataWrapper):
            data = DataWrapper(data)

        for _schema in schema.get('allOf', []):
            rs_component = _normalize(_schema, data)
            _merge_dict(result, rs_component)

        for key, _schema in six.iteritems(schema.get('properties', {})):
            # set default
            type_ = _schema.get('type', 'object')

            # get value
            value, has_key = data.get_check(key)
            if has_key:
                result[key] = _normalize(_schema, value)
            elif 'default' in _schema:
                result[key] = _schema['default']
            elif key in schema.get('required', []):
                if type_ in required_defaults:
                    result[key] = required_defaults[type_]
                else:
                    errors.append(dict(name='property_missing',
                                       message='`%s` is required' % key))

        additional_properties_schema = schema.get('additionalProperties', False)
        if additional_properties_schema is not False:
            aproperties_set = set(data.keys()) - set(result.keys())
            for pro in aproperties_set:
                result[pro] = _normalize(additional_properties_schema, data.get(pro))

        return result

    def _normalize_list(schema, data):
        result = []
        if hasattr(data, '__iter__') and not isinstance(data, dict):
            for item in data:
                result.append(_normalize(schema.get('items'), item))
        elif 'default' in schema:
            result = schema['default']
        return result

    def _normalize_default(schema, data):
        if data is None:
            return schema.get('default')
        else:
            return data

    def _normalize(schema, data):
        if schema is True or schema == {}:
            return data
        if not schema:
            return None
        funcs = {
            'object': _normalize_dict,
            'array': _normalize_list,
            'default': _normalize_default,
        }
        type_ = schema.get('type', 'object')
        if type_ not in funcs:
            type_ = 'default'

        return funcs[type_](schema, data)

    return _normalize(schema, data), errors
