#Orchestrator Backend
This python/flask application is the backend for the Aloha Orchestrator Project. All dependencies can be found in the `requirements.txt` file

This app was generated using the aloha backend-code-generator using the file `orchestrator.yml`, which has been provided.

##Setup and Running Application

###Installing Dependencies

Use a virtual environment, if desired

From inside the project folder:

`pip install -r requirements.txt` 



###Running App

_Confirm that mongo is running before attempting to start_

From inside the project folder:

`python __init__.py`


###Entities

#### Project
```
{
  id: string - MongoDB ObjectId for this Project
  title: string - Display name this Project
  description: string
  dataset: string
  constraints: string
  config: string
  algorithmConfig: string
  architectureSpec: string
  }
```
#### Card
```
{
  id: string - MongoDB ObjectId for this Card
  title: string - Display name this Card
  label: string - Display status of the associated Project
  description: string
  laneId: string - MongoDB ObjectId for the Lane containing this Card
  projectId: string - MongoDB ObjectId for the Project this Card describes
}
```
To move a card between lanes use **POST** `/cards/{cardId}` to update the Card. When the `laneId` of a card is updated 
through this call, the list of Cards on that lane is updated as well.

#### Lane
Container for each stage in the pipeline, essentially a list that holds multiple project cards
```
{
  id: string - MongoDB ObjectId for this Lane
  position: int - Index descri/bing the order of this Lane compared to other Lanes
  title: string - Display name for this lane
  cards: array <Card> (optional) - some API's will return the contained cards as a list for convenience
}
```
When starting the app for the first time on a cold database, Lanes must be created through the REST API to store the cards.


###Known Limitations

Multi-step process to create project, then create card for front end (lanes as well wehn db is totally cold)


When **POST** is made to `/project/{projectId}/status`, the `log` field is ignored

Project class for demo system overrides common Project class in models
