FROM frolvlad/alpine-python3

ENV API_SERVER_HOME=/opt/www
WORKDIR "$API_SERVER_HOME"
COPY "./requirements.txt" "./"
COPY "./" "./"



RUN cd /opt/www && \
    pip install -r requirements.txt




USER nobody
CMD [ "python3", "__init.py__" ]