import unittest
from mongoengine import *
from models.models import *
import os
import tempfile
from flask import Flask, url_for, redirect

from db.mongo import MongoConnector # Required to access db in this way
from optparse import OptionParser
from mongoengine.errors import ValidationError


#
# This file contains different examples of pytests. Please extend it to cover all your code.
#
# IMPORTANT! -> Testing functions name should start with:_ test_*
#
#
# To execute it:    python -m pytest test.py -s

class TestDesignPointCreation (unittest.TestCase):
    #Test Flask API initialization
    def test_flask_api(self):
        import api
        print("--> Test Flask API")
        app = Flask(__name__, static_folder='static')
        app.register_blueprint(
            api.bp,
            url_prefix='/api')

        @app.route('/')
        def home():
            return redirect(url_for('static', filename='swagger-ui/index.html'))
        c = app.test_client()

        app.config['MONGODB_SETTINGS'] = {
            'db': 'aloha', #Name of the database
            'host': "localhost", # Host of the DB
            'port': 27017, # DB port
            #'username':'aloha',
            #'password':'pwd123'
        }

        ## Start a new connection with the database (Required to use MongoEngine).
        mongo = MongoConnector(app)



        """
                
                
        2. COUNTING OBJECTS ON DB WHEN STARTING EXECUTION
        
        
        """

        PROJECT_ID = "5c09855522a6e337c1fa110d"

        designPoint = AlgorithmConfiguration()

        designPoint.project         = PROJECT_ID
        designPoint.onnx            = "DesignPoint/ONNX/path" # the path of the onnx model before the training


        # training
        designPoint.onnx_trained        = "DesignPoint/ONNX/path" # the path of the trained onnx model
        designPoint.pytorch_trained     = "DesignPoint/Pytorch/path" # the path of the trained pytotch model

        designPoint.training_accuracy   = 0.9
        designPoint.training_loss       = 0.21
        designPoint.validation_accuracy = 0.94
        designPoint.validation_loss     = 0.92


        # refinemente parsimonious inference
        designPoint.rpi_onnx_path           = "DesignPoint/RPI/ONNX/path" # the path for the onnx model after the rpi
        designPoint.rpi_training_accuracy   = 0.95
        designPoint.rpi_training_loss       = 0.84
        designPoint.rpi_onnx_img_path       = "/Users/dsolans/Documents/chart.png"


        # power performance evaluation
        designPoint.performance    = 4
        designPoint.energy         = 3
        designPoint.processors     = 2
        designPoint.memory         = 100


        # security
        designPoint.sec_level      = "Low"
        designPoint.sec_value      = 3
        designPoint.sec_curve      = {}


        # images from tools
        designPoint.training_img_path        = "/Users/dsolans/Documents/chart1.png"
        designPoint.power_perf_eval_img_path = "/Users/dsolans/Documents/chart2.png"
        designPoint.rpi_img_path             = "/Users/dsolans/Documents/chart3.png"
        designPoint.security_img_path        = "/Users/dsolans/Documents/chart4.png"

        designPoint.save()





