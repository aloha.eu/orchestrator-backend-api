# -*- coding: utf-8 -*-
from __future__ import absolute_import

from flask import Flask, url_for, redirect, request
from flask_socketio import SocketIO, emit, send

import api
from models.models import *
from db.mongo import MongoConnector
from settings import settings
from optparse import OptionParser
from flask_cors import CORS


def create_app():
    app = Flask(__name__, static_folder='static')
    app.register_blueprint(
        api.bp,
        url_prefix='/api')
    app.config['SECRET_KEY'] = 'secret'

    CORS(app)

    @app.route('/')
    def home():
        return redirect(url_for('static', filename='swagger-ui/index.html'))

    settings.get_settings(app)

    try:
        mongo = MongoConnector(app)

        init_user_count = User.objects.count()

        print("We initially had ", init_user_count, " users")

        if init_user_count == 0:
            user = User()
            user.email = "aloha-user@aloha-h2020.com"

            user.password = "hidden_pass"
            user.projects = ['3']
            user.save()

        init_lane_count = Lane.objects.count()

        lane_names = app.config['LANES']

        if init_lane_count != len(app.config['LANES']):

            mongo.db.drop_collection('lane')

            for lane_idx, lane_name in enumerate(lane_names):
                lane = Lane()
                lane.title = lane_name
                lane.position = lane_idx

                lane.save()

        # app.logger.info("Password: ", user.password)
    except Exception as ex:
        print(str(ex))
        # app.logger.info(str(ex))
        # app.logger.info("User %s was already created" % user.email)

    return app



if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-p", "--port", dest="port",
                      help="Flask port", metavar="", default=5000)

    (options, args) = parser.parse_args()
    print("Options: ", options)

    option_dict = vars(options)
    port = int(option_dict['port'])

    app = create_app()
    socketio = SocketIO(app=app, manage_session=True)


    @socketio.on('connect')
    def handle():
        print("connected")
        current_socket_id = request.sid
        emit('connect', {'socket_id': current_socket_id})

    socketio.run(app, debug=True, port=port)

